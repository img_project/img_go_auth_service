package main

import (
	"context"
	"log"
	"time"

	pbV1 "gitlab.com/img_project/img_go_auth_service/protobuf/auth_service/v1"
	"google.golang.org/grpc"
)

var (
	address = "localhost:7001"
)

func main() {
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pbV1.NewAuthServiceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.Login(ctx, &pbV1.LoginRequest{Username: "test_user", Password: "secret"})
	if err != nil {
		log.Fatalf("could not login: %v", err)
	}
	log.Printf("Access token: %s\nRefresh token: %s", r.GetAccessToken(), r.GetRefreshToken())

	r, err = c.RefreshToken(ctx, &pbV1.RefreshTokenRequest{RefreshToken: r.GetRefreshToken()})

	if err != nil {
		log.Fatalf("could not refresh token: %v", err)
	}

	log.Printf("Refreshed\nAccess token: %s\nRefresh token: %s", r.GetAccessToken(), r.GetRefreshToken())

	r1, err := c.VerifyToken(ctx, &pbV1.VerifyTokenRequest{Token: r.GetAccessToken()})

	if err != nil {
		log.Fatalf("could not verify token: %v", err)
	}

	if r1.Valid {
		log.Printf("valid token\n")
	} else {
		log.Printf("invalid token\n")
	}

	r1, err = c.VerifyToken(ctx, &pbV1.VerifyTokenRequest{Token: "123434"})

	if err != nil {
		log.Fatalf("could not verify token: %v", err)
	}

	if r1.Valid {
		log.Printf("valid token\n")
	} else {
		log.Printf("invalid token\n")
	}
}
