package main

import (
	"flag"
	"os"

	"github.com/jmoiron/sqlx"
	"gitlab.com/img_project/img_go_auth_service/pkg/config"
	"gitlab.com/img_project/img_go_auth_service/pkg/logger"
	pgStore "gitlab.com/img_project/img_go_auth_service/storage"
	"gitlab.com/img_project/img_go_auth_service/storage/repository"

	pgConn "gitlab.com/img_project/img_go_auth_service/connection/postgres"
)

var (
	username string
	password string
)

func init() {
	flag.StringVar(&username, "username", "test_user", "username for new user")
	flag.StringVar(&password, "password", "secret", "password for new user")

	flag.Parse()
}

func main() {
	// Load config from .env
	if err := config.New(os.Getenv("ENVIRONMENT")); err != nil {
		panic(err)
	}

	// Initialize customized zap logger
	logger.New(config.Get().App.LogLevel, config.Get().App.Name)

	// Initialize db connection
	conf := config.Get()
	dbConn, err := pgConn.NewPostgres(conf.Database.Postgres)

	if err != nil {
		panic(err)
	}
	defer pgConn.ClosePostgres(dbConn)

	// Get store
	store := getStore(dbConn)

	u := &repository.User{
		Username: username,
		Password: password,
		Status:   9,
		Blocked:  false,
	}

	if _, err := store.UserRepo().Create(u); err != nil {
		panic(err)
	}
}

func getStore(dbConn *sqlx.DB) pgStore.Storage {
	return pgStore.NewPostgresStorage(dbConn)
}
