package postgres

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/img_project/img_go_auth_service/pkg/config"
	"gitlab.com/img_project/img_go_auth_service/pkg/util"
)

func TestConnectionPostgres(t *testing.T) {
	util.LoadConfig("test", "../../.env")

	db, err := NewPostgres(config.Get().Database.Postgres)

	assert.NoError(t, err)
	assert.NotNil(t, db)

	assert.NoError(t, ClosePostgres(db))
}
