module gitlab.com/img_project/img_go_auth_service

go 1.16

require (
	github.com/MrWebUzb/goenv v1.0.3
	github.com/bxcodec/faker/v3 v3.6.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/google/uuid v1.3.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.2.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/stretchr/testify v1.7.0
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.7.0 // indirect
	go.uber.org/zap v1.18.1
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
	google.golang.org/grpc v1.40.0
	google.golang.org/protobuf v1.27.1
)
