package jwt

import "time"

func GenerateToken(username, secretKey string) (string, string, error) {
	j, err := NewJwt(secretKey)

	if err != nil {
		return "", "", err
	}

	accessToken, err := j.CreateToken(username, 24*time.Hour)

	if err != nil {
		return "", "", err
	}

	refreshToken, err := j.CreateToken(username, 2*24*time.Hour)

	if err != nil {
		return "", "", err
	}

	return accessToken, refreshToken, nil
}

func VerifyToken(token, secretKey string) (string, error) {
	j, err := NewJwt(secretKey)

	if err != nil {
		return "", err
	}

	p, err := j.VerifyToken(token)

	if err != nil {
		return "", err
	}

	return p.Username, nil
}
