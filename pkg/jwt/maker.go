package jwt

import (
	"errors"
	"fmt"
	"time"

	jwtGo "github.com/dgrijalva/jwt-go"
	imgVars "gitlab.com/img_project/img_go_auth_service/img_variables"
)

type Jwt struct {
	secretKey string
}

const minSecretKeySize = 32

func NewJwt(secretKey string) (*Jwt, error) {
	if len(secretKey) < minSecretKeySize {
		return nil, fmt.Errorf("invalid key size: must be at least %d characters", minSecretKeySize)
	}
	return &Jwt{secretKey}, nil
}

func (j *Jwt) CreateToken(username string, duration time.Duration) (string, error) {
	payload, err := NewPayload(username, duration)
	if err != nil {
		return "", err
	}

	jwtToken := jwtGo.NewWithClaims(jwtGo.SigningMethodHS256, payload)
	return jwtToken.SignedString([]byte(j.secretKey))
}

func (j *Jwt) VerifyToken(token string) (*Payload, error) {
	keyFunc := func(token *jwtGo.Token) (interface{}, error) {
		_, ok := token.Method.(*jwtGo.SigningMethodHMAC)
		if !ok {
			return nil, imgVars.ErrInvalidToken
		}
		return []byte(j.secretKey), nil
	}

	jwtToken, err := jwtGo.ParseWithClaims(token, &Payload{}, keyFunc)

	if err != nil {
		verr, ok := err.(*jwtGo.ValidationError)
		if ok && errors.Is(verr.Inner, imgVars.ErrExpiredToken) {
			return nil, imgVars.ErrExpiredToken
		}
		return nil, imgVars.ErrInvalidToken
	}

	p, ok := jwtToken.Claims.(*Payload)
	if !ok {
		return nil, imgVars.ErrInvalidToken
	}

	return p, nil
}
