package jwt

import (
	"time"

	"github.com/google/uuid"
	imgVars "gitlab.com/img_project/img_go_auth_service/img_variables"
)

type Payload struct {
	ID        string    `json:"id"`
	Username  string    `json:"username"`
	ExpiredAt time.Time `json:"expired_at"`
}

func NewPayload(username string, duration time.Duration) (*Payload, error) {
	tokenID, err := uuid.NewRandom()

	if err != nil {
		return nil, err
	}

	return &Payload{
		ID:        tokenID.String(),
		Username:  username,
		ExpiredAt: time.Now().Add(duration),
	}, nil
}

func (p *Payload) Valid() error {
	if time.Now().After(p.ExpiredAt) {
		return imgVars.ErrExpiredToken
	}
	return nil
}
