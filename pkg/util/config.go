package util

import (
	"gitlab.com/img_project/img_go_auth_service/pkg/config"
	"gitlab.com/img_project/img_go_auth_service/pkg/logger"
)

func LoadConfig(env, fileName string) {
	// Load config from .env
	if err := config.New(env, fileName); err != nil {
		panic(err)
	}

	// Initialize customized zap logger
	logger.New(config.Get().App.LogLevel, config.Get().App.Name)
}
