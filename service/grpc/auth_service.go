package grpc

import (
	"gitlab.com/img_project/img_go_auth_service/pkg/jwt"
	"gitlab.com/img_project/img_go_auth_service/pkg/logger"
	pbV1 "gitlab.com/img_project/img_go_auth_service/protobuf/auth_service/v1"
	"gitlab.com/img_project/img_go_auth_service/storage/repository"
	"google.golang.org/grpc/codes"
)

func (s *service) generateJwt(u *repository.User) (*pbV1.TokenResponse, error) {
	logger.Get().Info("generating access token and refresh token")
	accessToken, refreshToken, err := jwt.GenerateToken(u.Username, s.cfg.JwtSecret)

	if err != nil {
		return nil, logAndReturnError(codes.Internal, "internal server error: %v", err)
	}

	return &pbV1.TokenResponse{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}, nil
}
