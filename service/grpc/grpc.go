package grpc

import (
	"context"
	"fmt"
	"net"

	"github.com/jmoiron/sqlx"
	pgConn "gitlab.com/img_project/img_go_auth_service/connection/postgres"
	"gitlab.com/img_project/img_go_auth_service/pkg/config"
	"gitlab.com/img_project/img_go_auth_service/pkg/logger"
	serviceI "gitlab.com/img_project/img_go_auth_service/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	pbV1 "gitlab.com/img_project/img_go_auth_service/protobuf/auth_service/v1"
	pgStore "gitlab.com/img_project/img_go_auth_service/storage"
)

type service struct {
	db    *sqlx.DB
	store pgStore.Storage
	cfg   config.Application
	pbV1.UnimplementedAuthServiceServer
}

func NewService() (serviceI.Service, error) {
	conf := config.Get()
	dbConn, err := pgConn.NewPostgres(conf.Database.Postgres)

	if err != nil {
		return nil, err
	}

	return &service{
		db:    dbConn,
		cfg:   conf.App,
		store: pgStore.NewPostgresStorage(dbConn),
	}, nil
}

func (s *service) Run() error {
	defer func() {
		if err := pgConn.ClosePostgres(s.db); err != nil {
			panic(err)
		}
	}()

	listener, err := net.Listen("tcp", config.Get().App.Port)

	if err != nil {
		return err
	}

	srv := grpc.NewServer()
	defer srv.Stop()

	pbV1.RegisterAuthServiceServer(srv, s)

	logger.Get().Info("server listening on port", logger.String("port", s.cfg.Port))

	if err := srv.Serve(listener); err != nil {
		return err
	}

	return nil
}

func contextError(ctx context.Context) error {
	switch ctx.Err() {
	case context.Canceled:
		return logAndReturnError(codes.Canceled, "request is canceled")
	case context.DeadlineExceeded:
		return logAndReturnError(codes.DeadlineExceeded, "deadline is exceeded")
	default:
		return nil
	}
}

func logAndReturnError(code codes.Code, format string, args ...interface{}) error {
	logger.Get().Error("error", logger.Error(fmt.Errorf(format, args...)))
	return status.Errorf(code, format, args...)
}
