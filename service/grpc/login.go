package grpc

import (
	"context"

	imgVars "gitlab.com/img_project/img_go_auth_service/img_variables"
	"gitlab.com/img_project/img_go_auth_service/pkg/logger"
	"gitlab.com/img_project/img_go_auth_service/pkg/util"
	pbV1 "gitlab.com/img_project/img_go_auth_service/protobuf/auth_service/v1"
	"google.golang.org/grpc/codes"
)

func (s *service) Login(ctx context.Context, loginRequest *pbV1.LoginRequest) (*pbV1.TokenResponse, error) {
	logger.Get().Info("requesting to login")
	u, err := s.store.UserRepo().FindByUsername(loginRequest.GetUsername())

	if err != nil {
		if err == imgVars.ErrUserNotFound {
			return nil, logAndReturnError(codes.InvalidArgument, "incorrect username or password")
		}

		return nil, logAndReturnError(codes.Internal, "internal server error: %v", err)
	}

	if !util.ValidatePassword(loginRequest.Password, u.Password) {
		return nil, logAndReturnError(codes.InvalidArgument, "incorrect username or password")
	}

	return s.generateJwt(u)
}
