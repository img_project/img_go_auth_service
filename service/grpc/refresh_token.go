package grpc

import (
	"context"

	imgVars "gitlab.com/img_project/img_go_auth_service/img_variables"
	"gitlab.com/img_project/img_go_auth_service/pkg/jwt"
	"gitlab.com/img_project/img_go_auth_service/pkg/logger"
	pbV1 "gitlab.com/img_project/img_go_auth_service/protobuf/auth_service/v1"
	"google.golang.org/grpc/codes"
)

func (s *service) RefreshToken(ctx context.Context, refreshTokenRequest *pbV1.RefreshTokenRequest) (*pbV1.TokenResponse, error) {
	logger.Get().Info("requesting to refresh token")
	username, err := jwt.VerifyToken(refreshTokenRequest.GetRefreshToken(), s.cfg.JwtSecret)

	if err != nil {
		if err == imgVars.ErrExpiredToken || err == imgVars.ErrInvalidToken {
			return nil, logAndReturnError(codes.InvalidArgument, "invalid token")
		}
		return nil, logAndReturnError(codes.Internal, "internal server error: %v", err)
	}

	u, err := s.store.UserRepo().FindByUsername(username)

	if err != nil {
		if err == imgVars.ErrUserNotFound {
			return nil, logAndReturnError(codes.InvalidArgument, "invalid token")
		}

		return nil, logAndReturnError(codes.Internal, "internal server error: %v", err)
	}

	return s.generateJwt(u)
}
