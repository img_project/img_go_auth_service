package grpc

import (
	"context"

	imgVars "gitlab.com/img_project/img_go_auth_service/img_variables"
	"gitlab.com/img_project/img_go_auth_service/pkg/jwt"
	"gitlab.com/img_project/img_go_auth_service/pkg/logger"
	pbV1 "gitlab.com/img_project/img_go_auth_service/protobuf/auth_service/v1"
	"google.golang.org/grpc/codes"
)

func (s *service) VerifyToken(ctx context.Context, verifyTokenRequest *pbV1.VerifyTokenRequest) (*pbV1.VerifyTokenResponse, error) {
	logger.Get().Info("requesting to verify token")
	username, err := jwt.VerifyToken(verifyTokenRequest.GetToken(), s.cfg.JwtSecret)

	if err != nil {
		if err == imgVars.ErrExpiredToken || err == imgVars.ErrInvalidToken {
			return &pbV1.VerifyTokenResponse{Valid: false, UserId: ""}, nil
		}
		return nil, logAndReturnError(codes.Internal, "internal server error: %v", err)
	}

	u, err := s.store.UserRepo().FindByUsername(username)

	if err != nil {
		if err == imgVars.ErrUserNotFound {
			return &pbV1.VerifyTokenResponse{Valid: false}, nil
		}

		return nil, logAndReturnError(codes.Internal, "internal server error: %v", err)
	}

	return &pbV1.VerifyTokenResponse{
		Valid:  true,
		UserId: u.ID,
	}, nil
}
