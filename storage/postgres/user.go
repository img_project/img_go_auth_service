package postgres

import (
	"context"
	"database/sql"
	"time"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/img_project/img_go_auth_service/pkg/util"
	"gitlab.com/img_project/img_go_auth_service/storage/repository"

	imgVars "gitlab.com/img_project/img_go_auth_service/img_variables"
)

type userRepo struct {
	db *sqlx.DB
}

func NewUserRepo(db *sqlx.DB) repository.UserRepo {
	return &userRepo{db: db}
}

func (repo *userRepo) FindByUsername(username string) (*repository.User, error) {
	query := `SELECT * FROM users WHERE username=:username LIMIT 1`

	args := map[string]interface{}{
		"username": username,
	}

	var users []*repository.User

	rows, err := repo.db.NamedQueryContext(context.Background(), query, args)

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		u := &repository.User{}
		if err := rows.StructScan(u); err != nil {
			return nil, err
		}

		users = append(users, u)
	}

	if len(users) > 0 {
		return users[0], nil
	}

	return nil, imgVars.ErrUserNotFound
}

func (repo *userRepo) Create(user *repository.User) (string, error) {
	if err := repo.checkUserExistsByUsername(user.Username); err != nil {
		return "", err
	}

	query := `
		INSERT INTO users (
			id,
			username,
			password_hash,
			status,
			blocked
		) VALUES (
			:id,
			:username,
			:password,
			:status,
			:blocked
		)`

	id, err := uuid.NewRandom()

	if err != nil {
		return "", err
	}

	password, err := util.GenerateHash(user.Password, imgVars.DefaultBcryptHashCost)

	if err != nil {
		return "", err
	}

	user.Password = password

	args := map[string]interface{}{
		"id":       id.String(),
		"username": user.Username,
		"password": user.Password,
		"status":   user.Status,
		"blocked":  user.Blocked,
	}

	tx := repo.db.MustBeginTx(context.Background(), &sql.TxOptions{})

	_, err = tx.NamedExecContext(context.Background(), query, args)

	if err != nil {
		if err := tx.Rollback(); err != nil {
			return "", err
		}

		return "", err
	}

	return id.String(), tx.Commit()
}

func (repo *userRepo) Update(user *repository.User) error {
	if err := repo.checkUserExistsByIDNotEqualAndUsername(user.ID, user.Username); err != nil {
		return err
	}

	query := `
		UPDATE
			users
		SET
			username=:username,
			password_hash=:password,
			status=:status,
			blocked=:blocked,
			updated_at=:updated_at
		WHERE
			id=:id
		`

	password, err := util.GenerateHash(user.Password, imgVars.DefaultBcryptHashCost)

	if err != nil {
		return err
	}

	user.Password = password

	args := map[string]interface{}{
		"id":         user.ID,
		"username":   user.Username,
		"password":   user.Password,
		"status":     user.Status,
		"blocked":    user.Blocked,
		"updated_at": time.Now(),
	}

	tx := repo.db.MustBeginTx(context.Background(), &sql.TxOptions{})

	_, err = tx.NamedExecContext(context.Background(), query, args)

	if err != nil {
		if err := tx.Rollback(); err != nil {
			return err
		}

		return err
	}

	return tx.Commit()
}

func (repo *userRepo) Delete(id string) error {
	query := `DELETE FROM users WHERE id=:id`

	args := map[string]interface{}{
		"id": id,
	}

	tx := repo.db.MustBeginTx(context.Background(), &sql.TxOptions{})

	_, err := tx.NamedExecContext(context.Background(), query, args)

	if err != nil {
		if err := tx.Rollback(); err != nil {
			return err
		}

		return err
	}

	return tx.Commit()
}

func (repo *userRepo) checkUserExistsByIDNotEqualAndUsername(id, username string) error {
	query := `SELECT count(1) FROM users WHERE id<>$1 and username=$2`

	var cnt int

	err := repo.db.GetContext(context.Background(), &cnt, query, id, username)

	if err != nil {
		return err
	}

	if cnt > 0 {
		return imgVars.ErrUserExists
	}

	return nil
}

func (repo *userRepo) checkUserExistsByUsername(username string) error {
	query := `SELECT count(1) FROM users WHERE username=$1`

	var cnt int

	err := repo.db.GetContext(context.Background(), &cnt, query, username)

	if err != nil {
		return err
	}

	if cnt > 0 {
		return imgVars.ErrUserExists
	}

	return nil
}
