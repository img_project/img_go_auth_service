package repository

// User ...
type User struct {
	BaseSchema
	Username string `db:"username"`
	Password string `db:"password_hash"`
	Status   int    `db:"status"`
	Blocked  bool   `db:"blocked"`
}

// UserRepo user repository interface
type UserRepo interface {
	Create(*User) (string, error)
	Update(*User) error
	FindByUsername(username string) (*User, error)
	Delete(id string) error
}
