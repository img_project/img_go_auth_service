package storage

import (
	"github.com/jmoiron/sqlx"
	pgRepo "gitlab.com/img_project/img_go_auth_service/storage/postgres"
	"gitlab.com/img_project/img_go_auth_service/storage/repository"
)

// Storage interface for working with models
type Storage interface {
	UserRepo() repository.UserRepo
}

// storage implements Storage interface methods
type storage struct {
	db *sqlx.DB
}

// NewPostgresStorage creates new storage instance
func NewPostgresStorage(db *sqlx.DB) Storage {
	return &storage{db}
}

// GetUserRepo ...
func (s *storage) UserRepo() repository.UserRepo {
	return pgRepo.NewUserRepo(s.db)
}
