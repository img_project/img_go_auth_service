package postgres

import (
	"os"
	"testing"

	connectionPostgres "gitlab.com/img_project/img_go_auth_service/connection/postgres"
	"gitlab.com/img_project/img_go_auth_service/pkg/config"
	"gitlab.com/img_project/img_go_auth_service/pkg/util"
	"gitlab.com/img_project/img_go_auth_service/storage"
)

var store storage.Storage

func TestMain(m *testing.M) {
	util.LoadConfig("test", "../../../.env")

	db, err := connectionPostgres.NewPostgres(config.Get().Database.Postgres)

	if err != nil {
		panic(err)
	}

	defer func() {
		if err := connectionPostgres.ClosePostgres(db); err != nil {
			panic(err)
		}
	}()

	store = storage.NewPostgresStorage(db)

	os.Exit(m.Run())
}
