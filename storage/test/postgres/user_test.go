package postgres

import (
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/stretchr/testify/assert"
	"gitlab.com/img_project/img_go_auth_service/pkg/util"
	"gitlab.com/img_project/img_go_auth_service/storage/repository"
)

// create mock user
func createUser(t *testing.T, u *repository.User) {
	id, err := store.UserRepo().Create(u)

	assert.NoError(t, err)
	assert.NotEmpty(t, id)

	u.ID = id
}

// delete mock user
func deleteUser(t *testing.T, id string) error {
	return store.UserRepo().Delete(id)
}

func TestFindByUsername(t *testing.T) {
	testCases := []struct {
		name  string
		valid bool
	}{
		{
			name: "find by username",
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			user := repository.User{
				Username: faker.Username(),
				Password: faker.Password(),
				Status:   9,
				Blocked:  false,
			}

			createUser(t, &user)

			resultUser, err := store.UserRepo().FindByUsername(user.Username)

			assert.NoError(t, err)
			assert.Equal(t, user.Username, resultUser.Username)

			deleteUser(t, user.ID)
		})
	}
}

func TestCreateUser(t *testing.T) {
	testCases := []struct {
		name string
		user *repository.User
	}{
		{
			name: "create user",
			user: &repository.User{
				Username: faker.Username(),
				Password: faker.Password(),
				Status:   9,
				Blocked:  false,
			},
		},
		{
			name: "create user with sql injection",
			user: &repository.User{
				Username: faker.Username() + "' or 1=1--",
				Password: "1234",
				Status:   9,
				Blocked:  false,
			},
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			createUser(t, testCase.user)

			deleteUser(t, testCase.user.ID)
		})
	}
}

func TestUpdateUser(t *testing.T) {

	baseUser := &repository.User{
		Username: faker.Username(),
		Password: faker.Password(),
		Status:   9,
		Blocked:  false,
	}

	testCases := []struct {
		name           string
		user           *repository.User
		comparingField string
	}{
		{
			name: "update username",
			user: &repository.User{
				Username: faker.Username(),
				Password: baseUser.Password,
				Status:   baseUser.Status,
				Blocked:  baseUser.Blocked,
			},
			comparingField: "username",
		},
		{
			name: "update password",
			user: &repository.User{
				Username: baseUser.Username,
				Password: faker.Password(),
				Status:   baseUser.Status,
				Blocked:  baseUser.Blocked,
			},
			comparingField: "password",
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			createUser(t, baseUser)

			testCase.user.ID = baseUser.ID

			plainPassword := testCase.user.Password

			err := store.UserRepo().Update(testCase.user)

			assert.NoError(t, err)

			isValid := util.ValidatePassword(plainPassword, testCase.user.Password)

			switch testCase.comparingField {
			case "username":
				assert.NotEqual(t, testCase.user.Username, baseUser.Username)
			case "password":
				assert.Equal(t, true, isValid)
			default:
				assert.Equal(t, testCase.user.ID, baseUser.ID)
			}

			deleteUser(t, baseUser.ID)
		})
	}
}

func TestDeleteUser(t *testing.T) {
	u := &repository.User{
		Username: faker.Username(),
		Password: faker.Password(),
		Status:   9,
		Blocked:  false,
	}
	createUser(t, u)

	err := deleteUser(t, u.ID)

	assert.NoError(t, err)

	u1, err := store.UserRepo().FindByUsername(u.Username)

	assert.Error(t, err)
	assert.Equal(t, (*repository.User)(nil), u1)
}
